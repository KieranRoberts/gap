##########################################################################################
# Function name:  CoxeterGroupByDiagram
# Input: A diagram := (Number of vertices N, labelled edges visibleEdges_
# Ouput: A Coxeter group based on the input diagram
# Objective: Return a free group on N letters subject to relations defined by the edges in
#            the Coxeter diagram.
#
# Examples:
# Input: (5, [[1,2], [2,3], [3,4], [4,5]])
# Ouput: Coxeter group of type A_5
#
# Input: (4, [[1,2], [2,3], [3,4,2]]) (Note the extra '2' in the final vector implies that
#        there is a double edge.
# Output: Coxeter group of type B_4
#
# Input: (2,[[1,2,3]]). (Note the extra '3' in the vector is to implies that there is a 
#       triple edge.
#
##########################################################################################


CoxeterGroupByDiagram := function(N, visibleEdges)
    local  i, j, edges, temp, e, F, x, rels;


    # To begin with we will set edge[i][j] = 0 for all i,j since most pairs nodes do not have edges between them.
    edges := List([1..N], i->List([1..N], j->0));;

    # Now we change edges to add the visible edges.
    # If the size of a vector is 2, then it assumed that [i,j] is a single edge and so it is labelled with '1'
    # Otherwise the edge [i,j] is labelled by the third index of the vector (which should exist).
    for e in visibleEdges do
        if Size(e) = 2 then   
	        edges[e[1]][e[2]] := 1;
        else
            edges[e[1]][e[2]] := e[3];
       fi;
    od;

    
    # Now we define the Free group as follows. Each element on the node of the diagram will be represented by x[i].
    F := FreeGroup(N);
    x := GeneratorsOfGroup(F);

    # We define the relations between the generators.
    rels := [];

    #######################################################################    
    # All generators have order 2, i.e. x^2 = 1.
    # The relations between x[i] and x[j] are determined by the edge [i,j].
    # If there is no edge then the two corresponding node elements commute.
    # Otherwise, there is an edge labelled m=1,2,3, and
    # (xy)^k where k=3,4,6, respectively.
    ####################################################################### 

    for i in [1 .. N] do
        Add(rels, x[i]^2);
        for j in [i+1 .. N ] do
            if edges[i][j] = 0 then
                Add(rels, x[i]*x[j]*x[i]^-1*x[j]^-1);
            elif edges[i][j] = 1 then
                Add(rels, (x[i]*x[j])^3);
            elif edges[i][j] = 2 then
                Add(rels, (x[i]*x[j])^4);
            elif edges[i][j] = 3 then
                Add(rels, (x[i]*x[j])^6);
            else
                Error("Labels of edges must be 0,1,2,3\n");
            fi;
        od;
    od;
    return F/rels;
end;




##########################################################################################
# Function name:  CoxeterGroupByType
# Input: Type X of diagram, rank n of diagram.
# Ouput: Coxeter group of type X_n
# Objective: An (quasi-)overloader function for CoxeterGroupByDiagram. The function
#            constructs the Coxeter diagram of type (X,n).
#
# Example:
# Input: CoxeterGroupByType("A",5)
# Output: Coxeter group of type A_5.
##########################################################################################

CoxeterGroupByType := function(X, n);
    if (X = "A" and n>=2) then
        return CoxeterGroupByDiagram(n, List([1..n-1], i->[i,i+1]));
    elif (X = "B" and n>=2) then
        return CoxeterGroupByDiagram(n, Union(List([1..n-2], i->[i,i+1]),[[n-1,n,2]]));
    elif (X = "D" and n>=4) then
        return CoxeterGroupByDiagram(n, Union(List([1..n-3], i->[i,i+1]),[[n-2,n-1],[n-2,n]]));
    elif (X = "E" and n in [6,7,8]) then
        return CoxeterGroupByDiagram(n, Union(List([1..n-2], i->[i,i+1]),[[3,n]]));
    elif (X = "F" and n=4) then
        return CoxeterGroupByDiagram(n, [[1,2],[2,3,2],[3,4]]);
    elif (X = "G" and n=2) then
        return CoxeterGroupByDiagram(n, [[1,2,3]]);
    else
        Error("Please enter a valid irreducible Coxeter diagram.\n");
    fi;
end;




##########################################################################################
# Function name:  CoxeterGroup
# Three possible inputs:
# 1. As CoxeterGroupByDiagram.
# 2. As CoxeterGroupByType.
# 3. Multiple instances of the second type: Direct product of Coxter groups (which is again
#    a Coxeter group.
#
# Example of 3:
# Input: CoxeterGroup(['A',3], ['B', 5], ['A', 3]))
# Output: Returns the (reducible) Coxeter group of type A_3 x B_5 x A_3. 
##########################################################################################

CoxeterGroup := function(arg)
    if IsString(arg[1]) and IsInt(arg[2]) then
        return CoxeterGroupByType(arg[1],arg[2]);
    elif IsInt(arg[1]) and IsList(arg[2]) then
       return CoxeterGroupByDiagram(arg[1],arg[2]);
    elif IsList(arg[1]) then
      return DirectProduct(List([1..Size(arg)], i->CoxeterGroupByType(arg[i][1], arg[i][2]))); 
    else
       Error("Arguments do not match parameters\n");
    fi;
end;




##########################################################################################
# Function name:  ReflectionsOfGroup
# Input: Coxeter group G.
# Output: Reflections of group G.
#         Reflections are the conjugates of group elements corresponding to simple roots.
#
##########################################################################################

ReflectionsOfGroup := function(G)
    local gens, g, s, refs;

    gens := GeneratorsOfGroup(G);
    refs := Union(List(G,g->List(gens, s->s^g)));
    return refs;
end;




##########################################################################################
# Function name:  PermRepOfCoxeterGroup
# Input: Coxeter group G.
# Output: A faithful representation of G/Z(G).
#
# Notes:         
# Z(G) is trivial for types [A, n], [D, n odd] and E_6. All other centres have size 2. 
##########################################################################################

PermRepOfCoxeterGroup := function(G) # This should be a Coxeter group previously defined
    local T, Refs, g, Gperm, gens;
  
    T := reflectionsOf(G);
    Refs := MutableCopyMat(T);
    gens := GeneratorsOfGroup(G);
    Gperm := [];
    
    for g in gens do
        Refs := OnTuples(Refs,g);
        Add(Gperm,PermListList(T,Refs));
        Refs := MutableCopyMat(T);
    od;
    return Group(Gperm);
end;




##########################################################################################
# Function name:  SignedPermutationGroup 
# Input: Positive integer n>=2.
# Output: A faithful permutation representation with minimal degree of the Coxeter group 
#         of type B_n. 
#
# Notes:         
# The set of signed permutations on 2n letters.
##########################################################################################

SignedPermutationGroup := function(n)
    local i;
    
    return Group(Union(List([1..n-1], i->(i,i+1)(i+n,(i+1)+n)),[(1,n+1)]));
end;




##########################################################################################
# Function name:  EvenSignedPermutationGroup 
# Input: Positive integer n>=4.
# Output: A faithful permutation representation with minimal degree of the Coxeter group 
#         of type D_n. 
#
# Notes:         
# The set of evenly signed permutations on 2n letters. This is a subgroup of
# SignedPermutationGroup(n) with index 2.
##########################################################################################

EvenSignedPermutationGroup := function(n)
    local i;
    
    return Group(Union(List([1..n-1], i->(i,i+1)(i+n,(i+1)+n)),[(1,n+2)(2,n+1)]));
end;




##########################################################################################
# Function name: CoxeterElementOfGroup
# Input: Coxeter group G
# Output: A Coxeter element of G.
#
# Notes:         
# An element g of G is a Coxeter element if it contains every generator exactly once.
##########################################################################################

CoxeterElementOfGroup := function(G)
    return Product(GeneratorsOfGroup(G))^Random(G);
end;
