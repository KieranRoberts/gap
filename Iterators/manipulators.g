##########################################################################################
# Function name:  Swap
# Input: List or set.
# Ouput: List or set.
# Objective: Swaps the elements positioned at 'first' and 'second' of a list.
#
# Example:
# Input: ([5,7,1,3],1,4)
# Ouput: [3,7,1,5]
#
##########################################################################################

Swap := function(set, first, second)
    local temp;

    temp := set[first];
    set[first] := set[second];
    set[second] := temp; 
    
    return true;
    
end;




##########################################################################################
# Function name:  ReverseRightOf
# Input: List or set.
# Ouput: List or set.
# Objective: Reverses the order of elements of the subset [set[n+1], ... , set[Size(set)].
#
# Example:
# Input: ([2,4,5,6,1,4,8],4)
# Ouput: [2,4,5,6,8,4,1]
#
##########################################################################################

ReverseRightOf := function(set, n)
    local i, j;

    i := n+1; 
    j := Size(set);
    while (i<j) do
        Swap(set,i,j);
        i:=i+1;
        j:=j-1;
    od;

    return set;
end;




##########################################################################################
# Function name:  RightMostZero
# Input: List, binary
# Ouput: Nonnegative integer (index)
# Objective: Return the position of the right-most zero in a list or return 0 if no such 
#            element exists.
# Example:
# Input: [1,1,0,0,1,0,1]
# Ouput: 6
#
##########################################################################################

RightMostZero := function(I,n)
    local i;
    
    i := n;
    while i>0  do
        if I[i] = 0 then
            return i;
        fi;
        i := i-1;
    od;
    
    return i;
end;
