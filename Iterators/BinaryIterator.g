Read("manipulators.g");

##########################################################################################
# Function name: IsDoneIterator_BinaryNumbers
# Input: Iterator as given above.
# Ouput: True or false.
# Objective: Determines whether the iterator has arrived at its last element.
#
##########################################################################################

IsDoneIterator_BinaryNumbers := iter -> ( iter!.next = false );




##########################################################################################
# Function name: NextIterator_BinaryNumbers
# Input: Iterator, iter
# Ouput: The next element in iter
# Objective: Given an element in iter, determines the next element.
#
##########################################################################################

NextIterator_BinaryNumbers := function( iter )
    
    local rz,t,n,succ; # succ will be the successor to the element t in iter.
    
    t := iter!.next;  # Current element in iterator
    n := iter!.n;
    
    # if the current element is the last element in iter, then assign t's successor
    # to be false, i.e. it has no succesor.
    if t = iter!.last then
        succ := false; 
    
    ################################################################################################
    # The algorithm:
    # 1. Find the right-most zero in the binary string t and call its position rz.
    # 2.    If t has no zeroes, then succ = [1, 1, ... , 1] is the last element in iter.
    # 3.    Otherwise, set all 1s that appear to the left of succ[rz] to 0 and set succ[rz] to be 1.
    # 4. Assign iter's next element to be succ and return its previous element t.
    ################################################################################################
    
    else
        succ := ShallowCopy(t);
        rz := RightMostZero(t,n);
        if rz = 0 then
            iter!.next := false;
            break;    
        else
            succ{[rz+1..n]} := 0*[rz+1..n];
            succ[rz] := 1;
        fi;    
    fi;
    iter!.next := succ;
    return t;
end;




##########################################################################################
# Function name: ShallowCopy_BinaryNumbers
# Input: Iterator, iter
# Ouput: A distinct copy of iter
# Objective: A copy of the current iterator to be manipulated.
#
##########################################################################################

ShallowCopy_BinaryNumbers := 
    iter -> rec( n    := iter!.n,
                 last := iter!.last,
                 next := ShallowCopy( iter!.next ) );




##########################################################################################
# Function name: IteratorOfBinaryNumbers
# Input: Positive number, n.
# Ouput: A record of several functions: IsDoneIterator, NextIterator, ShallowCopy.
# Objective: To traverse through the all binary numbers of length n. The order of 
#            iteration is ascending order with respect to their decimal values.
#
##########################################################################################                 

IteratorOfBinaryNumbers := function(n) 
    
    if not (IsPosInt(n)) then
	Error( "The second argument <n> must be a non-negative integer" );
    fi; 

    return IteratorByFunctions(
      rec( IsDoneIterator := IsDoneIterator_BinaryNumbers,
           NextIterator   := NextIterator_BinaryNumbers,
           ShallowCopy    := ShallowCopy_BinaryNumbers,
           last           := List([1..n], i->1),                 
           n              := n,
           next           := 0*[1..n] ) );
end;
