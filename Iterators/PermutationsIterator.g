Read("manipulators.g");

##########################################################################################
# Function name: IsDoneIterator_Permutations
# Input: Iterator as given above.
# Ouput: True or false.
# Objective: Determines whether the iterator has arrived at its last element.
#
##########################################################################################

IsDoneIterator_Permutations := iter -> ( iter!.next = false );




##########################################################################################
# Function name: NextIterator_Permutations
# Input: Iterator, iter
# Ouput: The next element in iter
# Objective: Given an element in iter, determines the next element.
#
# Notes:
# Algorithm adapted from:
# http://alistairisrael.wordpress.com/2009/09/22/simple-efficient-pnk-algorithm/
##########################################################################################

NextIterator_Permutations := function( iter )
    
    local i,j,t,m,n,edge, succ;
    
    t := iter!.next; # Current element in the iterator.
    m := iter!.m;
    n := iter!.n;
    
    # if the current element is the last element in iter, then assign t's successor
    # to be false, i.e. it has no succesor.

    if t = iter!.last then
        succ := false;
    else
        succ := ShallowCopy(t);
        edge := n;
        if (n<=m) then
            j := n+1;
        # search for largest j such that succ[j] > succ[edge] (succ is increasing for j>=edge+1)
            while (j<=m and succ[edge]>=succ[j]) do
                j:=j+1;
            od;
        fi;

        if (n<=m and j<=m) then
            Swap(succ, edge, j);
        else
            if (n<=m) then
                ReverseRightOf(succ,n);
            fi;

            # find rightmost ascent to left of edge
            i := edge-1;
            while(i>=1 and succ[i]>=succ[i+1]) do 
                i:=i-1;
            od;

            if (i<=0) then
               iter!.next := false; 
               return t{[1..n]};
            fi;

            # find smallest j>i where succ[j]>succ[i] (succ is decreasing for j>=i+1)
            j := m;
        
            while(j>i and succ[i] >= succ[j]) do 
                j:=j-1;
            od;
        
            Swap(succ, i, j);
            ReverseRightOf(succ, i);
        fi;
   fi;
    iter!.next := succ;
    return t{[1..n]};
end;




##########################################################################################
# Function name: ShallowCopy_Permutations
# Input: Iterator, iter
# Ouput: A distinct copy of iter
# Objective: A copy of the current iterator to be manipulated.
#
##########################################################################################

ShallowCopy_Permutations := 
    iter -> rec( m    := iter!.m,
                 n    := iter!.n,
                 last := iter!.last,
                 next := ShallowCopy( iter!.next ) );




##########################################################################################
# Function name: IteratorOfPermutations
# Input: Set s, integer n.
# Ouput: A record of several functions: IsDoneIterator, NextIterator, ShallowCopy.
# Objective: To traverse through the all n-subsets of the set s. 
#
##########################################################################################    

IteratorOfPermutations := function( s, n )
    local m; 
    
    if not ( n=0 or IsPosInt( n ) ) then
	Error( "The second argument <n> must be a non-negative integer" );
    fi; 
	   
    if not ( IsCollection( s ) and IsFinite( s ) or IsEmpty( s ) and n=0 ) then
    	if s = [] then
    		return IteratorByFunctions(
      		  rec( IsDoneIterator := ReturnTrue,
                   NextIterator   := NextIterator_Permutations,
                   ShallowCopy    := ShallowCopy_Permutations,
                             next := false) );
    	else
		Error( "The first argument <s> must be a finite collection or empty" );
    	fi;
    fi;
    s := Set(s);
    m := Size(s);
    # from now on s is a finite set and n is its Cartesian power to be enumerated
    return IteratorByFunctions(
      rec( IsDoneIterator := IsDoneIterator_Permutations,
           NextIterator   := NextIterator_Permutations,
           ShallowCopy    := ShallowCopy_Permutations,
           m              := m,
           last           := [m,m-1..1],                 
           n              := n,
           next           := [1..m] ) );
end;
